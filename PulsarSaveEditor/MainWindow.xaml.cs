﻿using Microsoft.Win32;
using System;
using System.Windows;

namespace PulsarSaveEditor
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private PulsarSaveFile pulsarFile;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.CheckFileExists = true;
            openFileDialog.Multiselect = false;
            openFileDialog.Title = "Pick a PULSAR: Lost Colony save file.";

            openFileDialog.Filter = "PULSAR: Lost Colony save file|*.plsave";

            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles); //Environment.GetEnvironmentVariable("ProgramFiles");

            bool? result = openFileDialog.ShowDialog();
            if (result == true)
            {
                OpenFileName.Text = openFileDialog.FileName;
                pulsarFile = new PulsarSaveFile(openFileDialog.FileName);

                CREDITS_OLD.Text = pulsarFile.credits.ToString();
                CREDITS_NEW.Text = pulsarFile.credits.ToString();

                FUEL_OLD.Text = pulsarFile.fuel.ToString();
                FUEL_NEW.Text = pulsarFile.fuel.ToString();

                LEVEL_OLD.Text = pulsarFile.level.ToString();
                LEVEL_NEW.Text = pulsarFile.level.ToString();

                REP_CU_OLD.Text = pulsarFile.reputationCU.ToString();
                REP_CU_NEW.Text = pulsarFile.reputationCU.ToString();

                REP_AOG_OLD.Text = pulsarFile.reputationAOG.ToString();
                REP_AOG_NEW.Text = pulsarFile.reputationAOG.ToString();

                REP_WD_OLD.Text = pulsarFile.reputationWD.ToString();
                REP_WD_NEW.Text = pulsarFile.reputationWD.ToString();

                REP_FBC_OLD.Text = pulsarFile.reputationFBC.ToString();
                REP_FBC_NEW.Text = pulsarFile.reputationFBC.ToString();
            }
        }

        private void SaveAsButton_Click(object sender, RoutedEventArgs e)
        {
            if (pulsarFile != null)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();

                saveFileDialog.Title = "Save the new PULSAR: Lost Colony save file (must be different from the selected one).";

                saveFileDialog.Filter = "PULSAR: Lost Colony save file|*.plsave";

                saveFileDialog.InitialDirectory = pulsarFile.openFilePath;//Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles); //Environment.GetEnvironmentVariable("ProgramFiles");

                saveFileDialog.FileName = pulsarFile.openFilePath;

                bool? result = saveFileDialog.ShowDialog();
                if (result == true)
                {
                    if (pulsarFile != null)
                    {
                        pulsarFile.SaveAs(saveFileDialog.FileName,
                            pulsarFile.seed,
                            int.Parse(CREDITS_NEW.Text),
                            int.Parse(LEVEL_NEW.Text),
                            pulsarFile.experience,
                            pulsarFile.ironmode,
                            int.Parse(REP_CU_NEW.Text),
                            int.Parse(REP_AOG_NEW.Text),
                            int.Parse(REP_WD_NEW.Text),
                            int.Parse(REP_FBC_NEW.Text),
                            pulsarFile.gameName,
                            pulsarFile.shipName,
                            int.Parse(FUEL_NEW.Text)
                        );
                    }
                }
            }
            else
            {
                MessageBox.Show("You have to open a file first.", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
