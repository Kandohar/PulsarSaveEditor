﻿using System;
using System.IO;
using System.Windows;

namespace PulsarSaveEditor
{
    /// <summary>
    /// This is the main class to load a savefile and write it with modified values
    /// </summary>
    class PulsarSaveFile
    {
        public string openFilePath;

        public long seed;
        public int credits;
        public int level;
        public int experience;
        public bool ironmode;
        public int reputationCU;
        public int reputationAOG;
        public int reputationWD;
        public int reputationFBC;
        public string gameName;
        public string shipName;
        public int fuel;

        public PulsarSaveFile(string path)
        {
            openFilePath = path;

            FileStream fs = File.Open(openFilePath, FileMode.Open);
            StreamReader encoding = new StreamReader(fs, true);
            BinaryReader br = new BinaryReader(fs, encoding.CurrentEncoding);

            try
            {
                br.ReadInt32();
                br.ReadInt32();
                br.ReadInt32();
                br.ReadInt32();

                seed = br.ReadInt64();
                credits = br.ReadInt32();
                level = br.ReadInt32();
                experience = br.ReadInt32();

                br.ReadInt32();
                ironmode = br.ReadBoolean();

                reputationCU = br.ReadInt32();
                reputationAOG = br.ReadInt32();
                reputationWD = br.ReadInt32();
                reputationFBC = br.ReadInt32();

                br.ReadInt32();

                gameName = br.ReadString();
                shipName = br.ReadString();

                br.ReadInt32();
                fuel = br.ReadInt32();
            }
            catch (EndOfStreamException eose)
            {
                MessageBox.Show("Error while reading file. The file is perhaps corrupted.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception e)
            {
                MessageBox.Show("Error while reading file. " + e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            finally
            {
                br.Close();
                encoding.Close();
                fs.Close();
            }
        }

        /// <summary>
        /// Called by the UI when the "save as" button is pressed
        /// </summary>
        internal void SaveAs(string fileName,
            long Oseed, int Ocredits, int Olevel, int Oexperience, bool Oironmode,
            int OreputationCU, int OreputationAOG, int OreputationWD, int OreputationFBC,
            string OgameName, string OshipName, int Ofuel)
        {
            if (!fileName.Equals(openFilePath))
            {
                File.Copy(openFilePath, fileName);

                FileStream fs = File.Open(fileName, FileMode.Open);
                StreamReader encoding = new StreamReader(fs, true);
                BinaryReader br = new BinaryReader(fs, encoding.CurrentEncoding);
                BinaryWriter bw = new BinaryWriter(fs, encoding.CurrentEncoding);

                try
                {
                    br.ReadInt32();
                    br.ReadInt32();
                    br.ReadInt32();
                    br.ReadInt32();

                    bw.Write(Oseed);
                    bw.Write(Ocredits);
                    bw.Write(Olevel);
                    bw.Write(Oexperience);

                    br.ReadInt32();
                    bw.Write(Oironmode);

                    bw.Write(OreputationCU);
                    bw.Write(OreputationAOG);
                    bw.Write(OreputationWD);
                    bw.Write(OreputationFBC);

                    br.ReadInt32();

                    bw.Write(OgameName);
                    bw.Write(OshipName);

                    br.ReadInt32();
                    bw.Write(Ofuel);
                }
                catch (EndOfStreamException eose)
                {
                    MessageBox.Show("Error while reading file. The file is perhaps corrupted.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (Exception e)
                {
                    MessageBox.Show("Error while reading file. " + e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                }
                finally
                {
                    br.Close();
                    encoding.Close();
                    fs.Close();
                    bw.Close();
                }
            }
            else
            {
                MessageBox.Show("Cannot overwrite the same file.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
