# PulsarSaveEditor

## Summary

A tool to edit savefiles from the game: "Pulsar: Lost Colony" by Leafy Games, LLC

### Game version

This tool was tested with the version Beta 15.3.

### Folders

You can found the executable in the 'Application folder'.

The 'PulsarSaveEditor' is the main project with UI and save load feature.

The 'PulsarSaveExplorer' is a tool to help you to decrypt the savefile.

#### Software requirements

I use Visual Studio 2017 to develop these tools.

## Authors

* **Kandohar**

## License

This project is licensed under the MIT License.

## Acknowledgments

This project is not affiliated with Leafy Games, LLC.
