﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PulsarSaveExplorer
{
    /// <summary>
    /// This class allow you to explore the save file.
    /// I just tried to Read Int, String or Float until I found some relevent values
    /// </summary>
    class SaveExplorer
    {
        static void Main(string[] args)
        {
            FileStream fsIn1 = File.Open("Intrepid_begin.plsave", FileMode.Open);
            StreamReader encoding = new StreamReader(fsIn1, true);
            BinaryReader brIn1 = new BinaryReader(fsIn1, encoding.CurrentEncoding);

            FileStream fsIn2 = File.Open("Intrepid_1.plsave", FileMode.Open);
            BinaryReader brIn2 = new BinaryReader(fsIn2, encoding.CurrentEncoding);

            FileStream fsIn3 = File.Open("destroyer_1.plsave", FileMode.Open);
            BinaryReader brIn3 = new BinaryReader(fsIn3, encoding.CurrentEncoding);

            FileStream fsIn4 = File.Open("outrider_1.plsave", FileMode.Open);
            BinaryReader brIn4 = new BinaryReader(fsIn4, encoding.CurrentEncoding);

            FileStream fsIn5 = File.Open("ironmode_begin.plsave", FileMode.Open);
            BinaryReader brIn5 = new BinaryReader(fsIn5, encoding.CurrentEncoding);

            List<BinaryReader> readers = new List<BinaryReader>();

            readers.Add(brIn1);
            readers.Add(brIn2);
            readers.Add(brIn3);
            readers.Add(brIn4);
            readers.Add(brIn5);

            foreach (BinaryReader br in readers)
            {
                try
                {
                    // version ? timestamp ? date ? playtime ?
                    Write("", br.ReadInt32());
                    Write("", br.ReadInt32());
                    Write("", br.ReadInt32());
                    Write("", br.ReadInt32());

                    Write("SEED", br.ReadInt64());
                    Write("CREDITS", br.ReadInt32());
                    Write("LVL", br.ReadInt32());
                    Write("XP", br.ReadInt32());

                    Write("", br.ReadInt32());

                    Write("IRONMODE", br.ReadBoolean());

                    Write("REP CU", br.ReadInt32());
                    Write("REP AOG", br.ReadInt32());
                    Write("REP WD", br.ReadInt32());
                    Write("REP FBC", br.ReadInt32());
                    Write("", br.ReadInt32());

                    Write("GAME NAME", br.ReadString());
                    Write("SHIP NAME", br.ReadString());

                    Write("", br.ReadInt32());
                    Write("FUEL", br.ReadInt32());
                }
                catch (EndOfStreamException eose)
                {

                }

                Console.WriteLine("--------------------------------");
            }

            Console.Read();

        }

        private static void Write(String name, object value)
        {
            if (String.IsNullOrWhiteSpace(name))
            {
                name = "Unknown";
            }

            Console.WriteLine(String.Format("{0, -15}", name + ":") + value);
        }
    }
}
